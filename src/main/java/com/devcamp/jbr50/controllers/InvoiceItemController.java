package com.devcamp.jbr50.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr50.models.InvoiceItem;


@RestController
public class InvoiceItemController {
    @CrossOrigin
    @GetMapping(value="/invoices")
    public ArrayList<InvoiceItem> getInvoiceItem() {
        InvoiceItem invoiceItem1 = new InvoiceItem(1, "Hoa don tien nha", 1, 500000);
        InvoiceItem invoiceItem2 = new InvoiceItem(2, "Hoa don tien nuoc", 2, 600000);
        InvoiceItem invoiceItem3 = new InvoiceItem(3, "Hoa don tien dien", 5, 700000);
        ArrayList<InvoiceItem> listInvoiceItem = new ArrayList<InvoiceItem>();
        listInvoiceItem.add(invoiceItem1);
        listInvoiceItem.add(invoiceItem2);
        listInvoiceItem.add(invoiceItem3);

        return listInvoiceItem;

    }
    
}
